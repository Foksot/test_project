// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile/T_ProjectileBase.h"

// Sets default values
AT_ProjectileBase::AT_ProjectileBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void AT_ProjectileBase::BeginPlay()
{
	Super::BeginPlay();
	
}

