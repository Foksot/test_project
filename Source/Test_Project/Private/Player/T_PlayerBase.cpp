// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/T_PlayerBase.h"
#include "Components/T_Inventory.h"
#include "PickUpItem/T_PickUpItemBase.h"


// Sets default values
AT_PlayerBase::AT_PlayerBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(FName("MeshWeapon"));
	
	MeshWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("GripPoint"));
	Inventory = CreateDefaultSubobject<UT_Inventory>(FName("Inventory"));
}

// Called when the game starts or when spawned
void AT_PlayerBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AT_PlayerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AT_PlayerBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

//Interaction with objects, in this case only selection
void AT_PlayerBase::Interactive()
{
	//Pickup Item
	if (TargetItem)
	{
		TargetItem->Pickup(Inventory);		
	}

}

void AT_PlayerBase::SetNewWeaponMesh(USkeletalMesh* NewMesh)
{
	MeshWeapon->SetSkeletalMesh(NewMesh);
}

//Set the amount of damage dealt by the player
void AT_PlayerBase::SetDamage(int32 NewDamage)
{
	WeaponDamage = NewDamage;
}

//Player taking damage
void AT_PlayerBase::DamageTake(int32 NewDamage)
{
	HPinfo.HP -= NewDamage;
	if (HPinfo.HP <= 0)
	{
		DeathPlayer();
	}
}


void AT_PlayerBase::ClearTargetPickUpItem_Implementation()
{
	TargetItem = nullptr;
}

void AT_PlayerBase::SetTargetPickUpItem_Implementation(class AT_PickUpItemBase* NewTarget)
{
	TargetItem = NewTarget;
}


