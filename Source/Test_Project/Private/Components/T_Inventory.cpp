// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/T_Inventory.h"
#include "Player/T_PlayerBase.h"
#include "PickUpItem/T_PickUpTrash.h"
#include "PickUpItem/T_WeaponBase.h"
#include "Projectile/T_ProjectileBase.h"

// Sets default values for this component's properties
UT_Inventory::UT_Inventory()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UT_Inventory::BeginPlay()
{
	Super::BeginPlay();

	PlayerRef = Cast<AT_PlayerBase>(GetOwner());
	
	EmptySlotWeaponContainer = SizeWeaponContainer;
}

bool UT_Inventory::AddWeapon(FWeapon NewWeapon)
{
	//Checking if the inventory is full with weapons
	if (EmptySlotWeaponContainer == 0)
	{
		FWeapon DropWeapon;
		DropWeapon = WeaponConteiner[UseIndexWeapon];
		WeaponConteiner[UseIndexWeapon] = NewWeapon;

		//��� ������ ���� ���� ������!!!!
		return true;
	}

	int32 FreeSlot = -1;
	int32 IndexArray = 0;
	for (FWeapon FindWeapon : WeaponConteiner)
	{
		//Already in inventory
		if (FindWeapon.ClassWeapon == NewWeapon.ClassWeapon)
		{
			return false;
		}
		//Finding the first free slot
		else if (FreeSlot == -1 && !(FindWeapon.ClassWeapon))
		{
			FreeSlot = IndexArray;
		}

		IndexArray++;
	}
	WeaponConteiner[FreeSlot] = NewWeapon;
	if (FreeSlot == UseIndexWeapon)
	{
		PlayerRef->SetNewWeaponMesh(WeaponConteiner[FreeSlot].MeshWeapon);
	}
	PlayerRef->UpdateInfoWeaponEquipment();
	return true;
}

//Reloading weapons if possible
void UT_Inventory::ReloadAmmo()
{
	if (!(WeaponConteiner[UseIndexWeapon].ClassWeapon) || (WeaponConteiner[UseIndexWeapon].ClipSize == WeaponConteiner[UseIndexWeapon].MaxClipSize))
	{
		return;
	}
	int32 IndexArray = 0;
	for (FAmmo FindAmmo : AmmoConteiner)
	{
		if (FindAmmo.ClassAmmo == WeaponConteiner[UseIndexWeapon].NeedClassAmmo)
		{
			int32 NeedAmmo = WeaponConteiner[UseIndexWeapon].MaxClipSize - WeaponConteiner[UseIndexWeapon].ClipSize;
			if (FindAmmo.CountAmmo >= NeedAmmo)
			{
				WeaponConteiner[UseIndexWeapon].ClipSize += NeedAmmo;

				FindAmmo.CountAmmo -= NeedAmmo;
			}
			else
			{
				WeaponConteiner[UseIndexWeapon].ClipSize += FindAmmo.CountAmmo;

				FindAmmo.CountAmmo = 0;
			}
			if (FindAmmo.CountAmmo == 0)
			{
				AmmoConteiner.RemoveAt(IndexArray);
				return;
			}
			else
			{
				AmmoConteiner[IndexArray].CountAmmo = FindAmmo.CountAmmo;
				return;
			}
		}
		IndexArray++;
	}
}

//Reducing the amount of ammo when firing
void UT_Inventory::ReduceAmmoCount()
{
	WeaponConteiner[UseIndexWeapon].ClipSize -= 1;
}

//Getting information about the number of cartridges in an inventory of a certain type
int32 UT_Inventory::GetAllAmmo(TSubclassOf<AT_ProjectileBase> NeedClassAmmo)
{
	
	for (FAmmo FindAmmo : AmmoConteiner)
	{		
		if (FindAmmo.ClassAmmo == NeedClassAmmo)
		{
			
			return FindAmmo.CountAmmo;
		}
	}
	return 0;
}

int32 UT_Inventory::GetAllPriceTrash()
{
	if (TrashConteiner.Num() == 0)
	{
		return 0;
	}

	int32 PriceAll = 0;
	for (FTrash FindTrash : TrashConteiner)
	{
		PriceAll += (FindTrash.CountTrash * FindTrash.Price);
	}
	return PriceAll;
}

//Choosing a weapon from a new slot, changing the mesh and damage dealt
void UT_Inventory::SetUseWeapon(int32 NewIndex)
{
	UseIndexWeapon = NewIndex;
	PlayerRef->SetDamage(WeaponConteiner[NewIndex].Damage);
	PlayerRef->SetNewWeaponMesh(WeaponConteiner[NewIndex].MeshWeapon);
}

//Adding ammo to inventory
void UT_Inventory::AddAmmo(FAmmo NewAmmo)
{
	int32 IndexArray = 0;
	for (FAmmo FindAmmo : AmmoConteiner)
	{
		if (FindAmmo.ClassAmmo == NewAmmo.ClassAmmo)
		{
			AmmoConteiner[IndexArray].CountAmmo += NewAmmo.CountAmmo;
			PlayerRef->UpdateInfoWeaponEquipment();
			return;
		}
		IndexArray++;
	}

	AmmoConteiner.Add(NewAmmo);
	PlayerRef->UpdateInfoWeaponEquipment();
}

void UT_Inventory::AddTrash(FTrash NewTrash)
{
	int32 IndexArray = 0;
	for (FTrash FindTrash : TrashConteiner)
	{
		if (FindTrash.ClassTrash == NewTrash.ClassTrash)
		{
			TrashConteiner[IndexArray].CountTrash += NewTrash.CountTrash;
			PlayerRef->UpdateInfoTrash();
			return;
		}
		IndexArray++;
	}

	TrashConteiner.Add(NewTrash);
	PlayerRef->UpdateInfoTrash();
}

