// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUpItem/T_PickUpAmmo.h"
#include "Components/StaticMeshComponent.h"
#include "Components/T_Inventory.h"

AT_PickUpAmmo::AT_PickUpAmmo()
{
	MeshAmmo = CreateDefaultSubobject<UStaticMeshComponent>(FName("MeshAmmo"));
	MeshAmmo->SetupAttachment(RootComponent);
}

void AT_PickUpAmmo::Pickup(UT_Inventory* InventoryRef)
{
	Super::Pickup(InventoryRef);

	FAmmo NewInfoAmmo;

	NewInfoAmmo.ClassAmmo = AmmoInformation.ClassAmmo;
	NewInfoAmmo.CountAmmo = AmmoInformation.CountAmmo;
	InventoryRef->AddAmmo(NewInfoAmmo);

	Destroy();
}
