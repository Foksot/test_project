// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUpItem/T_WeaponBase.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/T_Inventory.h"

AT_WeaponBase::AT_WeaponBase()
{
	MeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(FName("MeshWeapon"));
	MeshWeapon->SetupAttachment(RootComponent);
}

void AT_WeaponBase::Pickup(UT_Inventory* InventoryRef)
{
	Super::Pickup(InventoryRef);

	FWeapon NewWeaponInformation;

	NewWeaponInformation.ClassWeapon = this->GetClass();
	NewWeaponInformation.NeedClassAmmo = WeaponInformation.NeedClassAmmo;
	NewWeaponInformation.Damage = WeaponInformation.Damage;
	NewWeaponInformation.ClipSize = WeaponInformation.ClipSize;
	NewWeaponInformation.MaxClipSize = WeaponInformation.MaxClipSize;
	NewWeaponInformation.MeshWeapon = MeshWeapon->SkeletalMesh;
	NewWeaponInformation.IconWeapon = WeaponInformation.IconWeapon;
	
	if (InventoryRef->AddWeapon(NewWeaponInformation))
	{
		Destroy();
	}
}
