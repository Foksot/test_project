// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUpItem/T_PickUpTrash.h"
#include "Components/StaticMeshComponent.h"
#include "Components/T_Inventory.h"

AT_PickUpTrash::AT_PickUpTrash()
{
	MeshTresh = CreateDefaultSubobject<UStaticMeshComponent>(FName("MeshTresh"));
	MeshTresh->SetupAttachment(RootComponent);
}

void AT_PickUpTrash::Pickup(UT_Inventory* InventoryRef)
{
	FTrash NewTrashInfo;

	NewTrashInfo.ClassTrash = GetClass();
	NewTrashInfo.CountTrash = Count;
	NewTrashInfo.Price = Price;

	InventoryRef->AddTrash(NewTrashInfo);

	Destroy();
}
