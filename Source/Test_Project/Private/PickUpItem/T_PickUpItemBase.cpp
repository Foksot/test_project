// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUpItem/T_PickUpItemBase.h"
#include "Components/BoxComponent.h"
#include "Interface/T_InterfaceTargetPickUp.h"

// Sets default values
AT_PickUpItemBase::AT_PickUpItemBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Trigger = CreateDefaultSubobject<UBoxComponent>(FName("Trigger"));
	SetRootComponent(Trigger);
	Trigger->OnComponentBeginOverlap.AddDynamic(this, &AT_PickUpItemBase::OnOverlapBegin);
	Trigger->OnComponentEndOverlap.AddDynamic(this, &AT_PickUpItemBase::OnOverlapEnd);
}

// Called when the game starts or when spawned
void AT_PickUpItemBase::BeginPlay()
{
	Super::BeginPlay();
	
}

//Passing a reference to an object that can be picked up
void AT_PickUpItemBase::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	IT_InterfaceTargetPickUp* InterfaceTarget = Cast<IT_InterfaceTargetPickUp>(OtherActor);
	if (InterfaceTarget) {
				
		InterfaceTarget->Execute_SetTargetPickUpItem(OtherActor,this);
	}

	
}

//Clearing a reference to an object that can be picked up
void AT_PickUpItemBase::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	IT_InterfaceTargetPickUp* InterfaceTarget = Cast<IT_InterfaceTargetPickUp>(OtherActor);
	if (InterfaceTarget) {

		InterfaceTarget->Execute_ClearTargetPickUpItem(OtherActor);
	}

}

void AT_PickUpItemBase::Pickup(UT_Inventory* InventoryRef)
{

}

