// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUpItem/T_PickUpItemBase.h"
#include "T_WeaponBase.generated.h"

class USkeletalMeshComponent;
class AT_ProjectileBase;

USTRUCT(BlueprintType)
struct FWeaponInformation
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<AT_ProjectileBase> NeedClassAmmo;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 Damage;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 ClipSize;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 MaxClipSize;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* IconWeapon;
};

UCLASS()
class TEST_PROJECT_API AT_WeaponBase : public AT_PickUpItemBase
{
	GENERATED_BODY()

public:

	AT_WeaponBase();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USkeletalMeshComponent* MeshWeapon;


public:

		virtual void Pickup(UT_Inventory* InventoryRef) override;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FWeaponInformation WeaponInformation;
};
