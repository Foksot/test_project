// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUpItem/T_PickUpItemBase.h"
#include "T_PickUpTrash.generated.h"

/**
 * 
 */
UCLASS()
class TEST_PROJECT_API AT_PickUpTrash : public AT_PickUpItemBase
{
	GENERATED_BODY()

public:
	AT_PickUpTrash();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* MeshTresh;

public:

	virtual void Pickup(UT_Inventory* InventoryRef) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Count;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Price;
	
};
