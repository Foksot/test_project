// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUpItem/T_PickUpItemBase.h"
#include "T_PickUpAmmo.generated.h"

class AT_ProjectileBase;

USTRUCT(BlueprintType)
struct FAmmoInformation
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<AT_ProjectileBase> ClassAmmo;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 CountAmmo;
};


UCLASS()
class TEST_PROJECT_API AT_PickUpAmmo : public AT_PickUpItemBase
{
	GENERATED_BODY()

public: 
	AT_PickUpAmmo();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* MeshAmmo;

public:

	virtual void Pickup(UT_Inventory* InventoryRef) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FAmmoInformation AmmoInformation;
};
