// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "T_Inventory.generated.h"

class AT_WeaponBase;
class AT_ProjectileBase;
class AT_PickUpTrash;
class USkeletalMesh;
class AT_PlayerBase;

USTRUCT(BlueprintType)
struct FWeapon
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<AT_WeaponBase> ClassWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<AT_ProjectileBase> NeedClassAmmo;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 Damage;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 ClipSize;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 MaxClipSize;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USkeletalMesh* MeshWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* IconWeapon;
};

USTRUCT(BlueprintType)
struct FAmmo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<AT_ProjectileBase> ClassAmmo;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 CountAmmo;
	
};

USTRUCT(BlueprintType)
struct FTrash
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<AT_PickUpTrash> ClassTrash;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 CountTrash;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 Price;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TEST_PROJECT_API UT_Inventory : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UT_Inventory();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	

	UFUNCTION(BlueprintCallable)
	bool AddWeapon(FWeapon NewWeapon);
	UFUNCTION(BlueprintCallable)
	void AddAmmo(FAmmo NewAmmo);
	UFUNCTION(BlueprintCallable)
	void AddTrash(FTrash NewTrash);


	UFUNCTION(BlueprintCallable)
	void SetUseWeapon(int32 NewIndex);

	UFUNCTION(BlueprintCallable)
		void ReloadAmmo();
	UFUNCTION(BlueprintCallable)
		void ReduceAmmoCount();
	UFUNCTION(BlueprintPure)
		int32 GetAllAmmo(TSubclassOf<AT_ProjectileBase> NeedClassAmmo);

	UFUNCTION(BlueprintPure)
	int32 GetAllPriceTrash();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FWeapon> WeaponConteiner;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FAmmo> AmmoConteiner;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FTrash> TrashConteiner;


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 SizeWeaponContainer = 4;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 EmptySlotWeaponContainer = 4;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 UseIndexWeapon = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AT_PlayerBase* PlayerRef;
		
};
