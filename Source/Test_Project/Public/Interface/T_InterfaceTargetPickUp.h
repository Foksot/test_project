// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "T_InterfaceTargetPickUp.generated.h"


class AT_PickUpItemBase;
class UT_Inventory;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UT_InterfaceTargetPickUp : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TEST_PROJECT_API IT_InterfaceTargetPickUp
{
	GENERATED_BODY()


public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SetTargetPickUpItem(AT_PickUpItemBase* NewTarget);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void ClearTargetPickUpItem();

};
