// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Interface/T_InterfaceTargetPickUp.h"
#include "T_PlayerBase.generated.h"

class AT_PickUpItemBase;
class UT_Inventory;

USTRUCT(BlueprintType)
struct FHPBar
{
	GENERATED_BODY()
		UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 HP = 100;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 MaxHP = 100;
	
};

UCLASS()
class TEST_PROJECT_API AT_PlayerBase : public ACharacter, public IT_InterfaceTargetPickUp
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AT_PlayerBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USkeletalMeshComponent* MeshWeapon;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UT_Inventory* Inventory;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
	void Interactive();
	UFUNCTION(BlueprintCallable)
	void SetNewWeaponMesh(USkeletalMesh* NewMesh);
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateInfoWeaponEquipment();
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateInfoTrash();
	UFUNCTION(BlueprintCallable)
	void SetDamage(int32 NewDamage);
	UFUNCTION(BlueprintImplementableEvent)
	void DeathPlayer();

	//Set new Target
	void SetTargetPickUpItem_Implementation(class AT_PickUpItemBase* NewTarget) override;

	//Clear Target
	void ClearTargetPickUpItem_Implementation() override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	AT_PickUpItemBase* TargetItem;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 WeaponDamage = 0;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FHPBar HPinfo;
	
	UFUNCTION(BlueprintCallable)
	void DamageTake(int32 NewDamage);

};
